package util

/**
 * Created with IntelliJ IDEA.
 * @author dhensche
 * Date: 3/21/13
 * Time: 4:12 PM
 */
class RichRange(targetRange: Range) {
  def quickSum: Long = {
    val lastTerm = targetRange.end - (targetRange.end % targetRange.step)
    val firstTerm = targetRange.start + (targetRange.step - (targetRange.start % targetRange.step))
    val n = ((lastTerm - firstTerm) / targetRange.step) + 1
    (0.5 * n * (firstTerm + lastTerm)).toLong
  }
}

object RichRangeImplicits {
  implicit def Range2RichRange(x: Range): RichRange = new RichRange(x)
}
