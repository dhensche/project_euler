package util

/**
 * Created with IntelliJ IDEA.
 * @author dhensche
 * Date: 3/29/13
 * Time: 9:57 AM
 */
object EulerMath {
  val (zero, one) = (BigInt(0), BigInt(1))

  lazy val fibs: Stream[BigInt] = {
    def loop(h: BigInt, n: BigInt): Stream[BigInt] = h #:: loop(n, h + n)
    loop(zero, one)
  }

  def gcd(a: BigInt, b: BigInt): BigInt = {
    if (b == zero) a.abs
    else gcd(b, a % b)
  }

  def lcm(a: BigInt, b: BigInt) = (a * b).abs / gcd(a, b)

  def from(start: BigInt): Stream[BigInt] = start #:: from(start + 1)

  /**
   * Returns all the prime factors sorted ascending by default. Pass in
   * asc=false if you want it descending. A good case for that is if you only
   * need the max value, this way you can just call .head on the returned Seq
   * instead of reversing or traversing the list.
   */
  def primeFactorization(n: BigInt, asc: Boolean = true): Seq[BigInt] = n match {
    case `one` => List()
    case _ => {
      val factor = from(2).find(n % _ == zero).get
      val next = n / factor
      asc match {
        case true => factor +: primeFactorization(next, asc)
        case false => primeFactorization(next, asc) :+ factor
      }
    }
  }
}
