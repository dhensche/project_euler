import problems._

/**
 * Created with IntelliJ IDEA.
 * @author dhensche
 * Date: 12/14/12
 * Time: 2:30 PM
 */
object Main {
  def main(args: Array[String]) {
    Problem1.solve
    Problem2.solve
    Problem3.solve
    Problem4.solve
    Problem5.solve
  }
}
