package problems

/**
 * Created with IntelliJ IDEA.
 *
 * This demonstrates the flatMap function. Basically this takes a function that has an argument type that matches the
 * collection type and a return type that is an iterable. This function is applied to each element in the original collection
 * and then the resulting iterables are flattened into a single collection
 * 
 * @author dhensche
 * Date: 4/1/13
 * Time: 1:12 PM
 */
object Problem4 extends Solvable[String] {
  val description: String = "find the largest palindrome that is a product of two 3-digit numbers"


  private[problems] def _solve: String = {
    val a = 999.until(100, -1)
    val b = 990.until(110, -11)
    (a flatMap {x => b map {y => x * y}}).filter(p => {
      val pString = p.toString
      pString == pString.reverse
    }).max.toString
  }

}
