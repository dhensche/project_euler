package problems

import util.EulerMath

/**
 * Created with IntelliJ IDEA.
 * @author dhensche
 * Date: 3/29/13
 * Time: 9:23 AM
 */
object Problem2 extends Solvable[BigInt] {
  val description: String = "find sum of even fibonacci numbers below 4 million"

  private[problems] def _solve: BigInt = {
    EulerMath.fibs.takeWhile(_ < 4000000).filter(_ % 2 == 0).fold(EulerMath.zero)(_ + _)
  }
}
