package problems

/**
 * Created with IntelliJ IDEA.
 * @author dhensche
 * Date: 12/14/12
 * Time: 1:50 PM
 */
trait Solvable[T] {
  val description: String

  def solve: T = {
    val start = System.currentTimeMillis()
    val result = _solve
    println("Time taken to %s: %dms".format(description, System.currentTimeMillis() - start))
    println("And the result is: %s".format(result))
    result
  }

  private[problems] def _solve: T
}
