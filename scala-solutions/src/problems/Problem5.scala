package problems

import util.EulerMath

/**
 * Created with IntelliJ IDEA.
 * @author dhensche
 * Date: 4/1/13
 * Time: 1:56 PM
 */
object Problem5 extends Solvable[BigInt] {
  val description: String = "find the lcm of all the numbers from 1 to 20"

  private[problems] def _solve: BigInt = EulerMath.from(1).take(20).fold(EulerMath.one){EulerMath.lcm}
}
