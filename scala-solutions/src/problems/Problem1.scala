package problems

import util.RichRangeImplicits._

/**
 * Created with IntelliJ IDEA.
 * @author dhensche
 * Date: 12/14/12
 * Time: 2:03 PM
 */
object Problem1 extends Solvable[Long] {
  val description: String = "find sum of conglomerate series with an exclusive limit of 1,000 and multiples of 3 and 5"

  private[problems] def _solve: Long = {
    val multiplicatives = List(3,5)

    val limit = 1000 - 1
    val lcms = multiplicatives.combinations(2).map(pair => pair(0) * pair(1))
    val sum = multiplicatives.foldLeft(0L)((result, mult) => result + 0.until(limit, mult).quickSum)
    sum - lcms.foldLeft(0L)((result, lcm) => result + 0.until(limit, lcm).quickSum)
  }
}
