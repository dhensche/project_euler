package problems

import util.EulerMath

/**
 * Created with IntelliJ IDEA.
 * @author dhensche
 * Date: 4/1/13
 * Time: 12:24 PM
 */
object Problem3 extends Solvable[BigInt] {
  val description: String = "find largest prime factor of 600,851,475,143"

  private[problems] def _solve: BigInt = EulerMath.primeFactorization(600851475143L, asc = false).head
}
