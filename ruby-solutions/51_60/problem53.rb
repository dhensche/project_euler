require "../utils/euler" if __FILE__ == $0

class Problem53
  def self.find_combs_over_x min
    combs = []
    (1..100).each do |n|
      (1..n).each do |r|
        comb = combination(n , r)
        combs << {:n => n, :r => r} if comb > min
      end
    end

    combs.length
  end

  def self.combination n, r
    return Euler.factorial(n) / (Euler.factorial(r) * Euler.factorial(n - r))
  end
end