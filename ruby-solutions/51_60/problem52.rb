require "../utils/euler" if __FILE__ == $0

class Problem52
  # the optimized limits are from http://www.mathblog.dk/project-euler-52-integer-same-digits/
  def self.find_lowest_multiple
    start = 1
    while true
      start *= 10
      ((start + 2)...(start * 10 / 6)).step(3) do |n|
        return n if self.same_digits_in_x_mults(n, 6)
      end

    end

    n
  end

  def self.same_digits_in_x_mults number, x
    (2..x).each do |n|
      return false if not Euler.is_perm2?(number, number * n)
    end

    true
  end
end