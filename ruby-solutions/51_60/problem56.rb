require "../utils/euler" if __FILE__ == $0

class Problem56
  def self.find_max_sum_of_digits
    max = 0
    99.downto(1) do |a|
      99.downto(1) do |b|
        pow = a ** b
        break if pow.to_s.length * 9 < max
        sum = Euler.soop_digits2(pow){|x| x}
        max = sum if sum > max
      end
    end

    max
  end
  # To change this template use File | Settings | File Templates.
end