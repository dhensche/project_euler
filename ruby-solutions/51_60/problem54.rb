require "set"

class Problem54

  def self.find_times_p1_wins file
    counts = {-1 => 0, 0 => 0, 1 => 0}
    while not file.eof?
      counts[determine_winner(file.readline)] += 1
    end

    counts[1]
  end

  def self.determine_winner row
    cards = row.split(" ")
    p1, p2 = cards[0..4], cards[5..-1]
    return hand_rank(p1) <=> hand_rank(p2)
  end

  def self.hand_rank hand
    ranks = card_ranks hand
    return [8, ranks.first] if straight(ranks) and flush(hand)
    return [7, kind(4, ranks), kind(1, ranks)] if kind(4, ranks)
    return [6, kind(3, ranks), kind(2, ranks)] if kind(3, ranks) and kind(2, ranks)
    return [5] + ranks if flush(hand)
    return [4, ranks.first] if straight(ranks)
    return [3, kind(3, ranks)] + ranks if kind(3, ranks)
    return [2] + two_pair(ranks) + ranks if two_pair(ranks)
    return [1, kind(2, ranks)] + ranks if kind(2, ranks)
    return [0] + ranks
  end
  def self.card_ranks hand
    ranks = hand.map{|c| '--23456789TJQKA'.index(c[0,1])}.sort{|x,y| y <=> x }
  end

  def self.flush hand
    suits = SortedSet.new(hand.map{|c| c[1,1]})
    suits.size == 1
  end

  def self.straight ranks
    ranks.first - ranks.last == 4 and ranks.uniq.size == 5
  end

  def self.kind n, ranks
    ranks.each do |r|
      return r if ranks.count(r) == n
    end
    nil
  end

  def self.two_pair ranks
    pair, lowpair = kind(2, ranks), kind(2, ranks.reverse)
    return [pair, lowpair] if pair and lowpair != pair
  end
  # To change this template use File | Settings | File Templates.
end