require "../utils/euler" if __FILE__ == $0

class Problem51
  #http://blog.dreamshire.com/2009/05/27/project-euler-problem-51-solution/
  def self.find_exchangeable_primes
    Euler.prime_sieve(1_000_000).each do |prime|
      if prime > 100_000
        s_prime = prime.to_s
        last_digit = s_prime[5,1]
        return s_prime if s_prime.count('0') == 3 and self.eight_prime_family(s_prime, '0')
        return s_prime if last_digit != '1' and s_prime.count('1') == 3 and self.eight_prime_family(s_prime, '1')
        return s_prime if s_prime.count('2') == 3 and self.eight_prime_family(s_prime, '2')
      end
    end
  end

  def self.eight_prime_family prime, replacement
    count = 0
    '0123456789'.each_char do |digit|
      n = prime.gsub(replacement, digit).to_i
      if n > 100_000 and Euler.is_prime? n
        count += 1
      end
    end

    return count == 8
  end
end