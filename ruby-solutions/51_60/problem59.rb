class Problem59
  def self.decrypt_file file, key_length
    chars = file.readline.split(",").map{|c| c.to_i}

    counter, key = Hash.new(0), []

    # this analyzes the encrypted message and finds the most common values for each index of the key
    # idea from http://www.mathblog.dk/project-euler-59-xor-encryption/
    chars.each_index do |i|
      j = i % key_length
      counter[[j, chars[i]]] += 1
      key[j] = chars[i] if counter[[j, chars[i]]] > counter[[j, key[j]]]
    end

    # this then transforms the most common values by the ascii value of space because that is the most common character in the English language
    # based on character frequency in the English language
    key = key.map{|x| x ^ " "[0]}

    # this zips up the chars with their key value
    char_pairs = chars.zip(key  * ((chars.length/ key_length) + 1))

    # this maps the pairs of encrypted values with key values to their XORed value (or original ascii value)
    msg_array = char_pairs.map{|pair| (pair[0] ^ pair[1])}

    # this maps the ascii values to the characters and joins them into a string
    msg = msg_array.map{|x| x.chr}.join

    puts msg
    return msg_array.reduce(:+)
  end
end