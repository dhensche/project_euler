class Problem57
  def self.find_uneven_fractions
    n_i, d_i, count = 3, 2, 0
    (0...1000).each do
      count += 1 if n_i.to_s.length > d_i.to_s.length
      n_i, d_i = 2 * d_i + n_i, d_i + n_i
    end

    count
  end
  # To change this template use File | Settings | File Templates.
end