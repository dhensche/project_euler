require "../utils/euler" if __FILE__ == $0

class Problem55
  def self.find_lychrel_numbers_below limit
    lychrels = []
    (1..limit).each do |n|
      i, s = 1, n + Euler.reverse(n)
      while i < 50 and not Euler.is_palindrome?(s)
        s = s + Euler.reverse(s)
        i += 1
      end
      lychrels << n if i == 50
    end

    lychrels
  end
  # To change this template use File | Settings | File Templates.
end