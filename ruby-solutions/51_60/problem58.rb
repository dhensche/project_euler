require "../utils/euler" if __FILE__ == $0

class Problem58
  def self.find_point_of_ten_percent
    prime_count, side_length, x = 3, 2, 9
    r_time = 0
    while prime_count.to_f / (2 * side_length + 1) > 0.10
      side_length += 2
      (0...3).each do
        x += side_length
        start = Time.now
        prime_count += 1 if Euler.is_pseudo_prime?(x)
        r_time += (Time.now - start)
      end
      x += side_length
    end
    "#{side_length + 1}"
  end
  # To change this template use File | Settings | File Templates.
end