require "../utils/euler" if __FILE__ == $0
require "set"

class Problem60

  def self.find_smallest_sum_of_5_concats
    primes, result = Euler.prime_sieve(20_000), 2 ** 30
    pairs = Array.new(primes.size)

    (1...primes.size).each do |a|
      break if primes[a] * 5 >= result
      pairs[a] = make_pairs(primes, a) if pairs[a].nil?

      ((a + 1)...primes.size).each do |b|
        break if primes[a] + primes[b] * 4 >= result
        next if not pairs[a].include?(primes[b])
        pairs[b] = make_pairs(primes, b) if pairs[b].nil?

        ((b + 1)...primes.size).each do |c|
          break if primes[a] + primes[b] + primes[c] * 3 >= result
          next if not pairs[a].include?(primes[c]) or not pairs[b].include?(primes[c])
          pairs[c] = make_pairs(primes, c) if pairs[c].nil?

          ((c + 1)...primes.size).each do |d|
            break if primes[a] + primes[b] + primes[c]  + primes[d] * 2 >= result
            next if not pairs[a].include?(primes[d]) or not pairs[b].include?(primes[d]) or not pairs[c].include?(primes[d])
            pairs[d] = make_pairs(primes, d) if pairs[d].nil?

            ((d + 1)...primes.size).each do |e|
              break if primes[a] + primes[b] + primes[c]  + primes[d] + primes[e] >= result
              next if not pairs[a].include?(primes[e]) or not pairs[b].include?(primes[e]) or not pairs[c].include?(primes[e])  or not pairs[d].include?(primes[e])

              result = primes[a] + primes[b] + primes[c] + primes[d] + primes[e] if primes[a] + primes[b] + primes[c] + primes[d] + primes[e] < result

              puts "#{primes[a]} + #{primes[b]} + #{primes[c]} + #{primes[d]} + #{primes[e]} = #{result}"
              break
            end
          end
        end
      end
    end

    result
  end

  def self.make_pairs primes, a
    pairs = Set.new
    ((a + 1)...primes.size).each do |b|
      pairs.add(primes[b]) if Euler.is_pseudo_prime?(Euler.concat(primes[a], primes[b])) and Euler.is_pseudo_prime?(Euler.concat(primes[b], primes[a]))
    end
    pairs
  end
  # To change this template use File | Settings | File Templates.
end