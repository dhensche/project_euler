class Euler
  @@factorial_cache = {0 => 1}

  def self.is_prime? n
    n = n.abs
    return true if n == 2 or n == 3
    return false if n < 2 or n%2 == 0
    return true if n < 9
    return false if n%3 == 0

    r = Math.sqrt(n).floor
    f = 5
    while f <= r
      return false if n % f == 0
      return false if n % (f + 2) == 0
      f += 6
    end
      return true
  end

  def self.is_pseudo_prime? n
    return false if n <= 1
    return true if n == 2
    return false if n % 2 == 0
    return true if n < 9
    return false if n % 5 ==0 or n % 3 == 0

    def self.modular_exp(a, b, n)
      d, k = 1, 0
      k += 1 while (b >> k) > 0
      (k - 1).downto(0).each do |i|
        d = d * d % n
        d = d * a % n if ((b >> i) & 1) > 0
      end

      d
    end

    def self.witness(a, n)
      t, u = 0, n - 1
      while (u & 1) == 0
        t += 1
        u >>= 1
      end

      xi1, xi2 = modular_exp(a, u , n), 0
      (0...t).each do |i|
        xi2 = xi1 * xi1 % n
        return true if (xi2 == 1) and (xi1 != 1) and (xi1 != (n - 1))
        xi1 = xi2
      end
      return true if xi1 != 1
      false
    end

    [2,3].each do |x|
      return false if witness(x, n)
    end
    true
  end

  def self.is_palindrome? n
    return n.to_s == n.to_s.reverse
  end

  def self.is_pandigital? n, length=9
    n_s = n.to_s
    return false if n_s.length != length

    (1..length).each do |d|
      return false if not n_s.include? d.to_s
    end

    true
  end

  def self.is_perm? n1, n2
    counts = Array.new(10, 0)
    (0..9).each do
      counts[n1 % 10] += 1
      counts[n2 % 10] -= 1
      n1 /= 10
      n2 /= 10
    end

    return counts.all?{|n| n == 0}
  end

  # so far this implementation is faster even though the first implementation supposedly runs in O(n) time and this
  # runs in O(n log n) time
  def self.is_perm2? n1, n2
    n2.to_s.chars.sort.join == n1.to_s.chars.sort.join
  end

  def self.prime_sieve n
    s = (0..n).to_a
    s[0] = s[1] = nil
    s.each do |p|
      next unless p
      break if p * p > n
      (p*p).step(n, p) { |m| s[m] = nil }
    end
    s.compact
  end

  # Sum of squares of digits in a number n
  def self.sos_digits n
    self.sop_digits n, 2
  end

  # Sum of Power of digits in a number n
  def self.sop_digits n, power
    self.soop_digits(n) {|x|
      x ** power
    }
  end

  def self.soop_digits n
    sum = 0
    while n != 0
      sum += yield(n % 10)
      n = n / 10
    end
    sum
  end

  def self.soop_digits2 n
    sum = 0
    n.to_s.chars.each do |x|
      sum += yield(x.to_i)
    end
    sum
  end

  def self.factorial n
    @@factorial_cache[n] = 1.upto(n).inject(:*) if not @@factorial_cache.has_key?(n)
    @@factorial_cache[n]
  end

  def self.rotate number, n=1
    a = number.to_s.chars.to_a
    rotate_arr(a, n).to_s.to_i
  end

  def self.rotate_arr arr, n=1
    head = arr.shift(n)
    (arr + head)
  end

  def self.concat x, y
    x * (10 ** (y.to_s.length)) + y
  end

  def self.reverse n
    n.to_s.reverse.to_i
  end

  def self.parse_file(file)
    file.readline.gsub(/"([A-Z]+?)"/, '\1').split(',')
  end

  def self.timeit times
    summate = []
    (0...times).each do
      s = Time.now
      yield()
      summate << (Time.now - s)
    end

    return summate.reduce(:+) / summate.size
  end

  def self.time_w_descr descr
    puts "Starting timed call"
    start = Time.now
    ans = yield()
    puts descr << ": #{Time.now - start} seconds"
    puts "Answer is: #{ans}\n-----------------------------------\n\n"
  end
end