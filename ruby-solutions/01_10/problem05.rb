class Problem05
  def self.smallest_dividable(limit)
    s = Time.now
    ans = (1..limit).inject(1) { |result, n| result.lcm n }
    puts "Time taken to find lcm of numbers 1 thru #{limit}: #{Time.now - s} seconds"
    ans
  end
end