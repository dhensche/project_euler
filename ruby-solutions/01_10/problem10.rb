class Problem10
  def self.sum_of_primes(limit)
    start = Time.now
    sieve = (0..limit).to_a
    sieve[0] = sieve[1] = nil
    sieve.each do |num|
      next unless num
      break if num * num > limit
      (num * num).step(limit, num) { |m| sieve[m] = nil }
    end
    sum = sieve.compact.reduce { |sum, m| sum += m }
    puts "Time taken to get the sum of all primes beneath #{limit}: #{Time.now - start} seconds"
    sum
  end
end