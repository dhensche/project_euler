class Problem06
  def self.diff_of_squares(max_num)
    start = Time.now
    sum1, sum2 = 0, 0
    (1..max_num).each do |num|
      sum1 += num ** 2
      sum2 += num
    end
    sum2 = sum2 ** 2
    result = sum2 - sum1
    puts "Time taken to find difference between sum of squares and square of sums for numbers 1 thru #{max_num}: #{Time.now - start} seconds"
    result
  end
end