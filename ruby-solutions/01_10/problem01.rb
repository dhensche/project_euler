class Problem01
  def self.sums_of_series(limit, *multiplicative) #only use prime numbers for multiplicative
    start = Time.now
    limit -= 1
    lcms = []
    multiplicative.combination(2) do |a|
      lcms << a[0] * a[1]
    end

    sum = multiplicative.reduce(0) { |sum, mult| sum += self.find_sum_of_series(limit, mult) }

    sub = lcms.reduce(0) { |sub, lcm| sub += self.find_sum_of_series(limit, lcm) }

    final = sum - sub
    puts "Time taken to find sum of conglomerate series with an exclusive limit of #{limit + 1} and multiples of #{multiplicative.join(",")}: #{Time.now - start} seconds"
    final
  end

  def self.find_sum_of_series(limit, diff)
    limit, diff = limit.to_f, diff.to_f
    n = (((limit - diff) / diff) + 1).floor
    (0.5 * n * diff *(1 + n)).floor
  end
end