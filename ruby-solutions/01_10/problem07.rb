class Problem07
  def initialize
    @count = 1
    @current = 3
    @primes = [2]
  end

  def find_x_primes(limit)
    start = Time.now
    while @count < limit
      if is_prime(@current)
        @count += 1
        @primes << @current
      end
      @current += 2
    end
    prime = @primes.last
    puts "Time taken to find all #{limit} primes: #{Time.now - start} seconds"
    prime
  end

  def is_prime(number)
    sqrt_number = Math.sqrt(number).floor
    @primes.each do |prime|
      break if prime > sqrt_number
      if number % prime == 0
        return false
      end
    end
    true
  end
end