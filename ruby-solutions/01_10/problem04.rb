class Problem04
  def self.largest_palindrome
    start = Time.now
    palindromes = []
    999.downto(100) do |i|
      110.step(i, 11) do |j|
        num = (i * j).to_s
        palindromes << num.to_i(10) if num == num.reverse
      end
    end
    max = palindromes.max
    puts "Time taken to find palindrome #{Time.now - start} seconds"
    max
  end
end