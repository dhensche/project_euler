class Problem08
  def initialize(n_string)
    @n_string = n_string
    @max = 0
  end

  def find_max_consec
    start = Time.now
    a, b, c, d, e = @n_string[0].to_i, @n_string[1].to_i, @n_string[2].to_i, @n_string[3].to_i, @n_string[4].to_i
    abcde = a * b * c * d * e
    @max = abcde
    count = 5

    while (n = @n_string[count]) != nil
      count += 1
      a, b, c, d, e = b, c, d, e, n.to_i
      abcde = a * b * c * d * e
      @max = [@max, abcde].max
    end
    puts "Time taken to find maximum consecutive product #{Time.now - start} seconds"
    @max
  end
end