class Problem02
  def self.sum_of_fib_evens(limit)
    start = Time.now
    a, b = 1, 1
    sum = 0
    while b <= limit
      sum += b if b % 2 == 0
      c = b
      b += a
      a = c
    end
    puts "Time taken to calculate the sum of all even fibonacci numbers not exceeding #{limit}: #{Time.now - start} seconds"
    sum
  end
end