class Problem03
  #stolen from http://www.benrady.com/2009/11/katarubyprime-factors.html after realizing the beauty
  def self.generate(n)
    return [] if n == 1
    factor = (2..n).find { |x| n % x == 0 }
    [factor] + generate(n / factor)
  end

  def self.largest_prime_factor(n)
    start = Time.now
    max= self.generate(n).max
    puts "Time taken to find largest prime factor of #{n}: #{Time.now - start} seconds"
    max
  end
end