class Problem09
  def self.find_triplet(num)
    start = Time.now
    ((num/5)..(num/2)).each do |i|
      (i..(num/2)).each do |j|
        c_2 = (i ** 2) + (j ** 2)
        c = Math.sqrt(c_2)

        if (c.to_i ** 2) == c_2
          if i + j + c == num
            puts "Found triplet in #{Time.now - start} seconds"
            return [i, j, c.to_i]
          end
        end
      end
    end
    puts 'Nothing found!'
  end
end