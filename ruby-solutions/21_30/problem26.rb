class Problem26
  def self.longest_cycle_under(limit)
    start = Time.now
    maximum = 0
    max_periodicity = 0
    primes = [2]

    (2..limit).each { |number|
      if Problem26.is_prime(number, primes)
        primes << number
        p = periodicity(number)
        if p > max_periodicity
          max_periodicity = p
          maximum = number
        end
      end
    }

    puts "Time taken to find longest recurring cycle for n where n is less than #{limit}: #{Time.now - start} seconds"
    maximum
  end

  def self.periodicity(number)
    remainder = 10
    remainders = [remainder]

    while true
      remainder = (remainder % number) * 10
      if remainders.include? remainder
        return remainders.size - remainders.index(remainder)
      end
      remainders << remainder
    end
  end

  def self.is_prime(number, primes)
    sqrt_number = Math.sqrt(number).floor
    primes.each do |prime|
      break if prime > sqrt_number
      if number % prime == 0
        return false
      end
    end
    true
  end
end