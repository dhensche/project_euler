class Problem23
  def self.sum_all_factors(number)
    prod = 1
    k = 2
    while (k * k) <= number
      p = 1
      while number % k == 0
        p = p * k + 1
        number /= k
      end
      prod *= p
      k += 1
    end

    prod *= (1 + number) if number > 1
    prod
  end

  def self.abundant_n_below(n)
    (1..n).to_a.keep_if { |item| self.sum_all_factors(item) > item + item }
  end

  def self.is_abundant(n)
    self.sum_all_factors(n) > n + n
  end

  def self.solve()
    start = Time.now
    limit = 20161
    all = [0]
    is_abundant = []
    abundants = []
    (1..limit).each do |n|
      all[n] = n
      is_abundant[n] = self.is_abundant(n)
      if is_abundant[n]
        abundants << n
      end
    end
    c = 0
    nab = abundants.length
    (1..limit).each do |n|
      i = 0
      while i < nab
        a = abundants[i]
        if n - a < 12
          break
        elsif is_abundant[n - a]
          all[n] = 0
          break
        end
        i += 1
      end
    end

    sum = all.reduce(:+)
    puts "Time taken to find all numbers that cannot be sum of two abundant numbers: #{Time.now - start} seconds"
    sum
  end
end