class Problem25
  @n_1, @n_2, @term = 1, 1, 2

  def self.next_fib
    t = @n_2
    @n_2 = @n_2 + @n_1
    @n_1 = t
    @term += 1
    @n_2
  end

  def self.find_x_digit_fib(x)
    start = Time.now
    while @n_2.to_s.length < x
      self.next_fib
    end
    puts "Time taken the brute way: #{Time.now - start} seconds"
    @term
  end

  def self.find_ans_fast
    start = Time.now
    a = (((999 * Math.log(10)) + (Math.log(5) / 2)) / Math.log((1 + Math.sqrt(5)) / 2)).ceil
    puts "Time taken the non-modifiable way: #{Time.now - start} seconds"
    a
  end
end