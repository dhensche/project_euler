require "../utils/euler" if __FILE__ == $0

class Problem22
  def self.parse_file(file)
    file.readline.gsub(/"([A-Z]+?)"/, '\1').split(',')
  end

  def self.total_word_scores(file)
    start = Time.now
    c = 0
    f_sum = Euler.parse_file(file).sort.reduce(0) do |sum, name|
      c += 1
      sum + c * name.each_byte.reduce(0) { |word_sum, byte| word_sum + byte - 64 }
    end
    puts "Time taken to summate word scores for given file: #{Time.now - start} seconds"
    f_sum
  end
end