require "set"

class Problem29
  def self.distinct_terms_in_range min, max
    start = Time.now
    set = SortedSet.new
    (min..max).each do |a|
      (min..max).each do |b|
        set.add(a ** b)
      end
    end

    count = set.size
    puts "Time taken to find the number of distinct terms for the equation a ** b where #{min} <= (a,b) <= #{max}: #{Time.now - start} seconds"
    count
  end
end