require "../utils/euler" if __FILE__ == $0

class Problem27
  def self.longest_string_of_primes(limit)
    start = Time.now
    odd_limit = limit.odd? ? limit : limit - 1
    a_max, b_max, n_max = 0, 0, 0
    primes = Euler.prime_sieve(limit)
    (-odd_limit).step(odd_limit, 2) { |a|
      primes.each { |b|
        n = 1
        while Euler.is_prime?(n ** 2 + n * a + b)
          n += 1
        end
        n_max, a_max, b_max = n, a, b if n > n_max
      }
    }

    puts "Time taken to find the quadratic expression that produces the maximum number of primes for consecutive values of n
where the coefficients must be |a| < #{limit}, |b| < #{limit}: #{Time.now - start} seconds "
    a_max * b_max
  end
end