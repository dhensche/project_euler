class Problem24
  def self.xnth_permutation(chars, x)
    start = Time.now
    chars = chars.split("").sort
    str = ''
    (1..chars.length).each do
      fact = (1..chars.length).reduce(:*)
      block_size = fact / chars.length
      block_passed = x / block_size
      str << chars.delete_at(block_passed)
      x = x % block_size
    end
    puts "#{Time.now - start}"
    str
  end
end