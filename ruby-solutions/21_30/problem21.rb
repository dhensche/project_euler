class Problem21

  def self.sum_factors(s, e)
    start = Time.now
    amicables = []
    (s..e).each do |a|
      b = self.sum_all_factors(a)
      amicables << a if b != a and self.sum_all_factors(b) == a
    end
    puts "Time taken to find all amicable numbers between #{s} and #{e}: #{Time.now - start} seconds"
    amicables.reduce(:+)
  end

  def self.sum_all_factors(number)
    n = 0
    sqrt_number = Math.sqrt(number).floor
    (1..sqrt_number).each do |i|
      if number % i == 0
        n += i
        if number / i != i
          n += number / i
        end
      end
    end
    n - number
  end
end