class Problem28
  def self.find_diag_sum(length)
    start = Time.now
    sum1 = sum2 = 1
    sum = 1
    (1..length - 1).each do |count|
      sum1 += count * 2
      sum2 += 2 * (count.odd? ? count + 1 : count)
      sum += sum1 + sum2
    end
    puts "Time taken to calculate sum of diagonals with side length of #{length}: #{Time.now - start} seconds"
    sum
  end
end