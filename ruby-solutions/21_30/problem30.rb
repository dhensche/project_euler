require "../utils/euler" if __FILE__ == $0

class Problem30
  def self.sum_of_numbers_as_power power
    start = Time.now
    sum = 0
    (2..self.find_max_value(power)).each do |n|
      sop = Euler.sop_digits(n, power)
      sum += sop if n == sop
    end

    puts "Time taken to find sum of all numbers where the sum of their digits to the power of #{power} equals the number: #{Time.now - start} seconds"
    sum
  end

  def self.find_max_value power
    length, n = 2, 1

    while length > n
      length = (n * (9 ** power)).to_s.length
      n += 1
    end
    n * (9 ** power)
  end
end