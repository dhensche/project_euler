class Problem64

  #http://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Continued_fraction_expansion
  def self.sq_rts_w_odd_periods_below bound
    periods = Array.new(bound, 0)
    (2..bound).each do |n|
      a0 = Math.sqrt(n).to_i
      next if a0 * a0 == n
      p, d, m, a = 0, 1, 0, a0

      begin
        m = d * a - m
        d = (n - m * m) / d
        a = (a0 + m) / d
        periods[n] += 1
      end while a != 2 * a0
    end

    periods.count{|period| period.odd?}
  end
  # To change this template use File | Settings | File Templates.
end