class Problem69
  def initialize
    @current = 3
    @primes = [2]
  end

  def find_n_over_phi_max(limit)
    start = Time.now
    product = 2
    while @current * product < limit
      if is_prime(@current)
        product *= @current
        @primes << @current
      end
      @current += 2
    end
    puts "Time taken to find largest n over phi value where n is less than #{limit}: #{Time.now - start} seconds"
    product
  end

  def is_prime(number)
    sqrt_number = Math.sqrt(number).floor
    @primes.each do |prime|
      break if prime > sqrt_number
      if number % prime == 0
        return false
      end
    end
    true
  end
end