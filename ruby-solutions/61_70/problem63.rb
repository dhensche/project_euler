class Problem63

  # The upper bound for x in x ** n is 9
  def self.find_n_digit_n_powers
    sum, lower, n = 0, 0, 1

    # while the lower bound is less than or equal to the upper bound
    while lower <= 9
      lower = (10 ** ((n - 1.0) / n)).ceil.to_i
      sum += 10 - lower
      n += 1
    end

    sum
  end
  # To change this template use File | Settings | File Templates.
end