require "../utils/euler" if __FILE__ == $0

=begin
     a
      \
        b       c
      /   \   /
    j       d
  /  \     /
 i    h---f-----e
       \
        g
=end

class Problem68
  def self.find_maximal_magic_gon
    solutions = []
    (1..10).to_a.permutation.each do |perm|
      a,b,c,d,e,f,g,h,i,j = perm

      # because for this to be maximal, the outer nodes must be greater than n / 2 (5 in this case)
      next if a < 6 or c < 6 or e < 6 or g < 6 or i < 6

      # because we have to start at the side with the outer node the lowest of the outer nodes
      next if a > c or a > e or a > g or a > i

      # because none of the inner nodes can be 10 and the length end up being 16
      next if b == 10 or d == 10 or f == 10 or h == 10 or j == 10
      next if a + b + d != c + d + f
      next if c + d + f != e + f + h
      next if e + f + h != g + h + j
      next if g + h + j != i + j + b
      solutions << [[a,b,d],[c,d,f],[e,f,h],[g,h,j],[i,j,b]].join.to_i
    end



    solutions.max
  end
end