class Problem61
  def initialize
    @result = Hash.new(0)
    @numbers = Hash.new([])
  end

  def find_cyclical_six
    "TSPXHO".chars.each {|poly| build_numbers_for_poly(poly)}

    @numbers["O"].each do |n|
      @result["O"] = n
      break if find_next("O", 1)
    end

    @result.keys.each do |k|
      puts "#{k}: #{@result[k]}"
    end

    @result.values.reduce(:+)
  end

  def find_next(last_type, length)
    "TSPXHO".chars.each do |i|
      next if @result[i] != 0
      (0...@numbers[i].size).each do |j|
        unique = true
        "TSPXHO".each do |k|
          if @numbers[i][j] == @result[k]
            unique = false
            break
          end
        end

        if unique and @numbers[i][j] / 100 == @result[last_type] % 100
          @result[i] = @numbers[i][j]
          return true if length == 5 and @result["O"] / 100 == @result[i] % 100
          return true if find_next(i, length + 1)
        end
      end
      @result[i] = 0
    end

    false
  end

  def build_numbers_for_poly(poly)
    n = 1
    while true
      p = 10_001
      p = n * (n + 1) / 2 if poly == "T"
      p = n ** 2 if poly == "S"
      p = n * (3 * n - 1) / 2 if poly == "P"
      p = n * (2 * n - 1) if poly == "X"
      p = n * (5 * n - 3) / 2 if poly == "H"
      p = n * (3 * n - 2) if poly == "O"
      n += 1
      next if p < 1_000
      break if p > 10_000
      @numbers[poly] += [p]
    end
  end
end