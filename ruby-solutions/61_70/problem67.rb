class Problem67
  def self.largest_path_sum(triangle)
    start = Time.now
    triangle = triangle.split("\n").map { |item| item.split(" ").map { |i| i.to_i } }
    last_row = triangle.reverse[0]
    max_sum = triangle.reverse[1..triangle.length].map do |row|
      c = 0
      row = row.map do |num|
        num += [last_row[c], last_row[c + 1]].max
        c += 1
        num
      end
      last_row = row
    end.reverse[0]
    puts "Time taken to generate largest sum of path elements for triangle with sides of length #{triangle.length}: #{Time.now - start} seconds"
    max_sum
  end
end