require "../utils/euler" if __FILE__ == $0
# we want to maximize the totient/n and to do that we need to limit the number of distinct prime factors of n
# because the totient is the product of the series of p|n (1- 1/p) where p are the distict prime factors of n
# and the product decreases with each prime introduced. To limit the number of prime factors we just try to find
# the solution n where n is the product of 2 co-prime numbers approaching 10 ** 7
# we can prove that a number with 3 prime factors has a higher ratio easily, but the permutation limit is slightly
# more complicated
class Problem70
  def self.fast_find_totient
    primes, min, min_result = Euler.prime_sieve(5_000), 10 ** 7, 0
    primes.each do |p1|
      next if p1 < 2000
      primes.each do |p2|
        next if p2 < 2000
        n = p1 * p2
        next if n > 10_000_000
        totient = (p1 - 1) * (p2 - 1)
        quotient = n / totient.to_f
        min, min_result = quotient, n if quotient < min and Euler.is_perm2?(n, totient)
      end
    end

    puts min
    puts euler_totient(min_result, primes)
    min_result
  end


  def self.find_minimizing_totient
    primes, min, min_result = Euler.prime_sieve(1_000_000), 10 ** 7, 0
    (2...(10 ** 7)).each do |n|
      totient = euler_totient(n, primes).to_i
      quotient = n / totient.to_f
      min, min_result = quotient, n if quotient < min and Euler.is_perm2?(n, totient)
    end

    puts min
    puts euler_totient(min_result, primes)
    min_result
  end

  def self.euler_totient n, primes
    totient, remain = n, n

    primes.each do |prime|
      # this is the line that speeds everything up
      return totient * (1 - 1.0/remain) if prime * prime > n

      pf = false
      while remain % prime == 0
        pf = true
        remain /= prime
      end

      totient *= (1 - 1.0/prime) if pf

      return totient if remain == 1
    end

    totient
  end
end