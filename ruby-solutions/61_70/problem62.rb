require "../utils/euler" if __FILE__ == $0
require "set"

class Problem62
  def self.find_smallest_cube_w_x_perms x
    n, counts, min_roots = 1, Hash.new(0), {}
    while true
      cube = n ** 3
      cs = cube.to_s.chars.to_a.sort
      counts[cs] += 1
      min_roots[cs] ||= n
      return min_roots[cs] ** 3 if counts[cs] == x
      n += 1
    end
  end
end