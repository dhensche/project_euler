require "../utils/euler" if __FILE__ == $0

class Problem65

  # The numerator follows the following equation
  # k % 3 == 0 : n_k = n_k_1 * (2 * (k / 3)) + n_k_2
  # else       : n_k = n_k_1 + n_k_2
  def self.sum_of_digits_for_convergent
    n_1k, n_k, = 1, 2
    (2..100).each do |i|
      n_2k, a_k = n_1k, i % 3 == 0 ? 2 * (i / 3) : 1
      n_1k = n_k
      n_k = a_k * n_1k + n_2k
    end

    Euler.soop_digits(n_k){|x| x}
  end
  # To change this template use File | Settings | File Templates.
end