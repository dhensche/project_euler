class Problem20
  def self.sum_of_fact(lim)
    start = Time.now
    s = (2..lim).reduce(:*).to_s.split('').reduce(0) { |sum, n| sum += n.to_i }
    puts "Time taken to find sum of digits in #{lim}!: #{Time.now - start} seconds"
    s
  end
end