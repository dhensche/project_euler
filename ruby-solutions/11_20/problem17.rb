class Problem17
  @hundred = 7
  @thousand = 8
  @and = 3

  @under_twenty = [4, 3, 3, 5, 4, 4, 3, 5, 5, 4, 3, 6, 6, 8, 8, 7, 7, 9, 8, 8]
  @decades = [6, 6, 5, 5, 5, 7, 6, 6]

  def self.number_word_length(number) # works up to 9999
    n = number
    length = 0
    while n > 0
      if n > 9999
        return "To Big!!"
      end
      if n >= 1000
        rem = n % 1000
        length += @under_twenty[n / 1000] + @thousand
        n = rem
      elsif n >= 100
        rem = n % 100
        length += @under_twenty[n / 100] + @hundred
        length += @and if rem > 0
        n = rem
      elsif n >= 20
        rem = n % 10
        length += @decades[(n / 10) - 2]
        n = rem
      elsif n > 0
        length += @under_twenty[n]
        n = 0
      end
    end
    length
  end

  def self.sum_of_lengths(s, e)
    start = Time.now
    sum = (s..e).to_a.reduce(0) { |sum, n| sum += self.number_word_length(n) }
    puts "Time taken to find the sum of the lengths of numbers (in words) from #{s} to #{e} (inclusive): #{Time.now - start} seconds"
    sum
  end
end