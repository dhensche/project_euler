class Problem16
  def self.sum_of_digits(number)
    start = Time.now
    sum = number.to_s.split('').reduce(0) { |sum, n| sum += n.to_i }
    puts "Time taken to find the sum of the digits in #{number}: #{Time.now - start} seconds"
    sum
  end
end