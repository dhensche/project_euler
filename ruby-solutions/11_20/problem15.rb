class Problem15
  def self.number_of_routes(max)
    start = Time.now
    paths = self.pascals_triangle(max, 2 * max).to_i
    puts "Time taken to find the number of paths in a #{max} by #{max} grid: #{Time.now - start} seconds"
    paths
  end

  def self.pascals_triangle(x, y)
    if x == 0
      return 1
    end
    return self.pascals_triangle(x - 1, y) * ((y + 1.0 - x) / x)
  end
end