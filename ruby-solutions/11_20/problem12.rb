class Problem12
  def self.t_number_x_factors(x)
    start = Time.now
    step = 1
    count = 0
    while count < x
      step += 1
      if step % 2 == 0
        count = self.count_all_factors(step / 2) * self.count_all_factors(step + 1)
      else
        count = self.count_all_factors(step) * self.count_all_factors((step + 1) / 2)
      end
    end
    puts "Time taken to find smallest triangle number with at least #{x} factors: #{Time.now - start} seconds"
    (step * (step + 1)) / 2
  end

  def self.count_all_factors(number)
    n = 0
    sqrt_number = Math.sqrt(number).floor
    (1..sqrt_number).each do |i|
      if number % i == 0
        n += 2
      end
      if number / i == i
        n -= 1
      end
    end
    n
  end
end