class Problem11
  def self.max_linear_product(matrix_str, length)
    start = Time.now
    matrix = matrix_str.split("\n").map { |line| line = line.split(" ").map { |number| number.to_i } }
    length -= 1
    products = []
    matrix.each_index do |y|
      matrix[y].each_index do |x|
        products << self.calc_vert(matrix, x, y, length)
        products << self.calc_horiz(matrix, x, y, length)
        products << self.calc_diag_right(matrix, x, y, length)
        products << self.calc_diag_left(matrix, x, y, length)
      end
    end
    max = products.max
    puts "Time taken to find the maximum product of #{length + 1} consecutive numbers either vertically, horizontally or diagonally in a "\
    "#{matrix[0].length} by #{matrix.length} grid: #{Time.now - start} seconds"
    max
  end

  def self.calc_vert(matrix, x, y, length)
    return 0 if (y + length >= matrix.length)

    return (0..length).reduce(1) { |prod, num| prod *= matrix[y + num][x] }
  end

  def self.calc_horiz(matrix, x, y, length)
    return 0 if (x + length >= matrix[0].length)

    return (0..length).reduce(1) { |prod, num| prod *= matrix[y][x + num] }
  end

  def self.calc_diag_right(matrix, x, y, length)
    return 0 if (y + length >= matrix.length or x + length >= matrix[0].length)

    return (0..length).reduce(1) { |prod, num| prod *= matrix[y + num][x + num] }
  end

  def self.calc_diag_left(matrix, x, y, length)
    return 0 if (y + length >= matrix.length or x - length < 0)

    return (0..length).reduce(1) { |prod, num| prod *= matrix[y + num][x - num] }
  end
end