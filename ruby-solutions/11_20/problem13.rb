class Problem13
  def self.list_summation(list, digits_requested)
    start = Time.now
    ans = list.split("\n").map { |str| str[0..digits_requested].to_i }.reduce { |sum, add| sum += add }.to_s[0..9]
    puts "Time taken to find first 10 digits of a list of #{list.split("\n").length} 50 digit numbers: #{Time.now - start} seconds"
    ans
  end
end