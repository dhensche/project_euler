require 'date'
class Problem19
  def self.num_of_sun(s, e)
    start = Time.now
    c = 0
    while s < e
      c += 1 if s.wday == 0
      s = s.next_month
    end
    puts "Time taken to count months that start on a sunday from #{s} to #{e}: #{Time.now - start} seconds"
    c
  end
end