class Problem14
  @cache = {1 => 1}

  def self.max_length_series(limit)
    start = Time.now
    max = 0
    max_start = 1
    (1..limit - 1).each do |n|
      m = self.collatz(n)
      if m > max
        max = m
        max_start = n
      end
    end
    puts "Time taken to find longest collatz for all numbers 1 to #{limit}: #{Time.now - start} seconds"
    {start => max_start, length => max}
  end

  def self.collatz(n)
    if @cache.has_key? n
      return @cache[n]
    end
    @cache[n] = self.collatz(n.even? ? n / 2 : 3 * n + 1) + 1
  end
end