require "../utils/euler" if __FILE__ == $0
require "set"

class Problem49
  def self.find_fancy_terms
    primes, all = SortedSet.new(Euler.prime_sieve(10_000)), Set.new
    primes.each do |n|
      if n > 1000
        specials = SortedSet.new
        n.to_s.chars.to_a.map(&:to_i).permutation do |p|
          if p[0] != 0
            p_n = p.reduce{|x, y| Euler.concat(x, y)}
            specials.add(p_n) if primes.include?p_n
          end
        end

        if specials.size >= 3
          specials.each do |previous|
            specials.each do |p|
              if p != previous and specials.include?(p + (p - previous))
                all.add([previous, p , p + (p - previous)].sort)
              end
            end
          end
        end
      end
    end

    all.to_a.map{|a| a.reduce{|x, y| Euler.concat(x, y)}}.join(",")
  end
end