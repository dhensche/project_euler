require "problem44" if __FILE__ == $0


class Problem45

  def self.find_hex_pen_tri_from_285
    i, result = 285, 0

    while true
      i += 1
      result = i * (i + 1) / 2
      break if Problem44.is_pentagonal?(result) and Problem45.is_hexagonal?(result)
    end

    result
  end

  def self.is_hexagonal? x
    sqrt = Math.sqrt(8 * x + 1)
    sqrt == sqrt.floor and sqrt % 4 == 3
  end
  # To change this template use File | Settings | File Templates.
end