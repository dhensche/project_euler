require "../utils/euler" if __FILE__ == $0
require "set"

class Problem46
  def self.find_smallest_invalid_prime
    # started out with this at 1_000 and it wasn't enough so I increased until I found a high enough value. After finding
    # the solution I reduced it to marginally increase performance by finding only the primes I needed
    primes = Euler.prime_sieve 6_000
    p_set, i, result = Set.new(primes), 9, 0

    while true
      i += 2
      next if p_set.include? i

      x = 1
      x_2 = self.x_squared_and_doubled(x)
      while x_2 < i and not p_set.include?(i - x_2)
        x += 1
        x_2 = self.x_squared_and_doubled(x)
      end

      if x_2 > i
        result = i
        break
      end
    end

    result
  end

  def self.x_squared_and_doubled x
    (2 * (x ** 2))
  end
  # To change this template use File | Settings | File Templates.
end