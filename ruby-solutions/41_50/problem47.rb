require "../utils/euler" if __FILE__ == $0
require "set"

class Problem47
  def self.find_first_four_consecutive
    i, count, primes = 1, 0, SortedSet.new(Euler.prime_sieve(100_000))
    while true
      if primes.include? i
        count = 0
      elsif self.uniq_prime_factors(i, primes) == 4
        count += 1
      else
        count = 0
      end

      break if count == 4
      i += 1
    end

    i - 3
  end

  def self.uniq_prime_factors n, primes
    nof, remain = 0, n

    primes.each do |prime|
      # this is the line that speeds everything up
      return 1 + nof if prime * prime > n

      pf = false
      while remain % prime == 0
        pf = true
        remain /= prime
      end

      nof += 1 if pf

      return nof if remain == 1
    end

    nof
  end

  # To change this template use File | Settings | File Templates.
end