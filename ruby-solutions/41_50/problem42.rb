require "../utils/euler" if __FILE__ == $0

class Problem42
  def self.find_triangle_words file
    start = Time.now
    words, triangular_words = Euler.parse_file(file), []

    words.each do |word|
      sqrt = Math.sqrt(8 * (word.each_byte.map{|x| x - 64}.reduce(:+)) + 1)
      triangular_words << word if sqrt == sqrt.floor && sqrt.floor.odd?
    end

    puts "Time taken to find all triangle words in file: #{Time.now - start} seconds"
    triangular_words
  end
end