require "../utils/euler" if __FILE__ == $0

class Problem41

  # can skip 9 and 8 digit pandigitals because the sum of their digits is divisible by 9
  def self.largest_pandigital_prime
    start = Time.now
    result = 0

    7_654_321.downto(0) do |x|
      if Euler.is_pandigital?(x, x.to_s.length) && Euler.is_prime?(x)
        result = x
        break
      end
    end

    puts "Time taken to find largest pandigital prime: #{Time.now - start} seconds"
    result
  end
end