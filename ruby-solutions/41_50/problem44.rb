class Problem44
  def self.find_special_pentagonals
    not_found, i, result = true, 1, 0
    while not_found
      i += 1
      n = i * (3 *  i - 1) / 2

      (i-1).downto(1) do |j|
        m = j * (3 * j - 1) / 2

        if self.is_pentagonal?(n - m) and self.is_pentagonal?(n + m)
          result = n - m
          puts "i: #{i}, j: #{j}"
          not_found = false
          break
        end
      end
    end

    result
  end

  # from http://www.divye.in/2012/07/how-do-you-determine-if-number-n-is.html
  def self.is_pentagonal? x
    sqrt = Math.sqrt(24 * x + 1)
    sqrt == sqrt.floor and sqrt % 6 == 5
  end
end