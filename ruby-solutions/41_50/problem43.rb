require "../utils/euler" if __FILE__ == $0

class Problem43
  def self.find_numbers_matching
    start = Time.now
    numbers, matching = (0..9).to_a, []
    (1..9).each do |i|
      head = numbers.delete_at(i)
      puts head
      numbers.permutation.each do |p|
        n_a = p.unshift(head)

        next if n_a[5] != 5
        next if n_a[3] % 2 != 0

        if self.matches_rule n_a
          matching << n_a.reduce{|x, y| Euler.concat(x, y)}
        end
      end

      numbers.insert(i, i)
    end

    puts "Time taken to find numbers matching the rule: #{Time.now - start} seconds"
    matching
  end

  def self.matches_rule digits
    divisors = [1, 2, 3, 5, 7, 11, 13, 17]
    (1...divisors.length).each do |i|
      num = 100 * digits[i] + 10 * digits[i + 1] + digits[i + 2]
      return false if num % divisors[i] != 0
    end

    true
  end
end