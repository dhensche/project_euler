class Problem48
  def self.find_last_10_digits
    result, modulo = 0, 10_000_000_000

    (1..1000).each do |i|
      temp = i
      (1...i).each do |j|
        temp *= i
        temp %= modulo
      end

      result += temp
      result %= modulo
    end

    result
  end
  # To change this template use File | Settings | File Templates.
end