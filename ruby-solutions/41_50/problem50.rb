require "../utils/euler" if __FILE__ == $0
require "set"

class Problem50
  # After finding a less than optimal solution I went to http://blog.dreamshire.com/2009/04/12/project-euler-problem-50-solution/
  # for inspiration. Realized that (a + b + c + d) - (a + b) = (c + d) is the logic used here. That is why I can just
  # look at the difference of sums and their indexes in the sum array
  def self.find_prime_as_sum_under limit
    primes, prime_sum = Euler.prime_sieve(limit), [0]

    count, sum, terms, max_prime, jm, im = 0, 0, 1, 0, 0, 0
    while sum < limit
      sum += primes[count]
      prime_sum << sum
      count += 1
    end

    (0...count).each do |i|
      (i + terms...count).each do |j|
        n = prime_sum[j] - prime_sum[i]
        if j - i > terms and Euler.is_prime? n
          terms, max_prime = j - i, n
          jm, im = j, i
        end
      end
    end

    " Total of #{terms} terms where the sum is: #{max_prime} = #{primes[im...jm].join(" + ")}"
  end
end