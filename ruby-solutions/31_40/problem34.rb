require "../utils/euler" if __FILE__ == $0

class Problem34
  def self.sum_of_factorions
    start = Time.now
    facts, sum = [1] + (1..9).map {|x| Euler.factorial x}, 0
    (10..40_731).each do |n|
      sof = Euler.soop_digits(n) {|x| facts[x]}
      if sof == n
        sum += n
      end
    end
    puts "Time taken to find sum of all factorians with optimized limits: #{Time.now - start} seconds"
    sum
  end
end