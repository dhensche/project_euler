require "../utils/euler" if __FILE__ == $0

class Problem38
  # based on logic at http://www.mathblog.dk/project-euler-38-pandigital-multiplying-fixed-number/
  def self.largest_pandigital_from_concat
    start = Time.now
    result = 0
    (9234..9387).reverse_each do |i|
      result = Euler.concat(i, 2 * i)
      break if Euler.is_pandigital?(result)
    end
    puts "Time taken to find the largest pandigital number from concatination: #{Time.now - start} seconds"

    result
  end
end