require "rational"

class Problem33
  def self.find_non_trivial_reductions
    start = Time.now
    n_i = [1, 0]
    n_prod, d_prod = 1, 1
    (10..99).each do |numerator|
      ((numerator + 1)..99).each do |denominator|
        n_s, d_s = numerator.to_s, denominator.to_s

        index = 0 if d_s[1] == n_s[0]
        index = 1 if d_s[0] == n_s[1]
        if index != nil
          if (n_s[n_i[index],1].to_f / d_s[index,1].to_f) == (numerator.to_f / denominator.to_f)
            n_prod *= n_s[n_i[index],1].to_i
            d_prod *= d_s[index,1].to_i
          end
        end
      end
    end

    denom = d_prod / d_prod.gcd(n_prod)
    puts "Time taken to find non-trivial trick reductions and then the reduced denominator from their product: #{Time.now - start} seconds"
    denom
  end
end