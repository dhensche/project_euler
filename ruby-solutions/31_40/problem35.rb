require "../utils/euler" if __FILE__ == $0
require "set"

class Problem35
  def self.find_circular_primes_below x
    start = Time.now
    primes = SortedSet.new(Euler.prime_sieve x)
    circular = SortedSet.new

    primes.each do |prime|
      if prime < 10
        circular << prime
        next
      end

      length, count = prime.to_s.length, 0

      (1..length).each do |n|
        other = Euler.rotate(prime, n)
        if primes.include? other
        else
          break
        end
        count += 1
      end
      if count == length
        circular << prime
      end
    end

    puts "Time taken to find number of circular primes below #{x}: #{Time.now - start} seconds"
    circular.size
  end
end