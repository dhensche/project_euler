require "../utils/euler" if __FILE__ == $0
require "set"

class Problem37
  def self.sum_of_truncatable_primes
    start = Time.now
    primes, i = Euler.prime_sieve(750_000), 4
    prime_set = SortedSet.new primes
    truncatable = []

    while truncatable.size < 11
      right, left, m = primes[i], 0, 1
      is_prime = true

      while right > 0 && is_prime
        left += m * (right % 10)
        is_prime = prime_set.include?(left) && prime_set.include?(right)
        right /= 10
        m *= 10
      end

      if is_prime
        truncatable << primes[i]
      end

      i += 1
    end

    sum = truncatable.reduce(:+)

    puts "Time taken to find sum of truncatable primes: #{Time.now - start} seconds"
    sum
  end
end