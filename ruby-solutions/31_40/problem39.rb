class Problem39

=begin
  a**2 + b**2 = c**2
  a + b+ c = p

  yields:
    b = (p**2 -2*p*a) / (2*p-2*a)
=end
  def self.find_p_with_max_triples_under x
    start = Time.now
    result = 0, max_solutions = 0
    2.step(x, 2) do |perimeter|
      solutions = 0
      (2...(perimeter/3)).each do |a|
        solutions += 1 if (perimeter * (perimeter - (2 * a))) % (2 * (perimeter - a)) == 0
      end

      if solutions > max_solutions
        max_solutions = solutions
        result = perimeter
      end
    end

    puts "Time taken to find a perimeter under #{x} with a maximum number of pythagorean triples: #{Time.now - start} seconds"
    result
  end
end