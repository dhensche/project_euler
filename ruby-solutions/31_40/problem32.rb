require "set"
require "../utils/euler" if __FILE__ == $0

class Problem32
  # limits stolen from dreamshire
  def self.sum_of_pandigital_mults
    start = Time.now
    pandigitals = SortedSet.new
    (2..99).each do |i|
      s = i > 9 ? 123 : 1234
      (s..10_000/i).each do |j|
        pandigitals.add(i * j) if Euler.is_pandigital?(i.to_s + j.to_s + (i * j).to_s)
      end
    end
    sum = pandigitals.reduce(:+)
    puts "Time taken to find sum of unique pandigital multiplicative equations: #{Time.now - start} seconds"
    sum
  end
end