class Problem36
  @sum = 0

  def self.sum_of_palindromes(limit)
    start = Time.now
    (1..limit - 1).each { |x|
      if x.odd?
        y = x.to_s
        if y == y.reverse
          y = x.to_s(2)
          if y == y.reverse
            @sum += x
          end
        end
      end
    }
    puts "Took #{Time.now - start} seconds to find the sum of all base 10 and 2 palindromes under #{limit}"
    @sum
  end
end