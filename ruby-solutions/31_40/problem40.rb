require "../utils/euler" if __FILE__ == $0

class Problem40
  def self.find_product_of_irrational digits
    start = Time.now
    n, s, m = 1, ""

    while s.length < digits.max
      s << n.to_s
      n += 1
    end
    product = digits.map{|x| s[x-1,1].to_i}.reduce(:*)
    puts "Time taken to find product of the #{digits.join(", ")} digits: #{Time.now - start} seconds"
    product
  end
end