class Problem31
  def self.ways_to_make_change denominations, goal
    start = Time.now
    ways = Array.new(goal + 1, 0)
    ways[0] = 1
    denominations.each do |coin|
      (coin..goal).each do |i|
        ways[i] += ways[i - coin]
      end
    end

    puts "Time taken to find the total number of ways to get to #{goal} using the denomations [#{denominations.reduce {|x, y| x.to_s + ', ' + y.to_s}}]: #{Time.now - start} seconds"
    ways[200]
  end
end