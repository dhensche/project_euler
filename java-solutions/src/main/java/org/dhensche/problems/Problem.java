package org.dhensche.problems;

/**
 * @author dhensche
 *         Date: 8/10/15
 */
public interface Problem {
    String description();
    void solve();
}
