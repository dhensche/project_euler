package org.dhensche.problems;

import com.google.common.io.Files;
import com.google.common.io.LineProcessor;
import org.dhensche.util.Graph;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author dhensche
 *         Date: 8/10/15
 */
public class Problem81 implements Problem {

    public String description() {
        return "In the 5 by 5 matrix below, the minimal path sum from the top left to the bottom right, by only moving to the right and down, is indicated in bold red and is equal to 2427.\n" +
            "\n" +
            "⎛⎝⎜⎜⎜⎜⎜⎜131201630537805673968036997322343427464975241039654221213718150111956331⎞⎠⎟⎟⎟⎟⎟⎟\n" +
            "Find the minimal path sum, in matrix.txt (right click and \"Save Link/Target As...\"), a 31K text file containing a 80 by 80 matrix, from the top left to the bottom right by only moving right and down.";
    }

    public void solve() {
        try {
            URL resource = getClass().getClassLoader().getResource("p081_matrix.txt");
            if (resource != null) {
                List<List<Integer>> matrix = Files.readLines(new File(resource.toURI()), Charset.defaultCharset(), new LineProcessor<List<List<Integer>>>() {
                    List<List<Integer>> rows = new ArrayList<>();

                    public boolean processLine(String line) throws IOException {
                        List<Integer> row = StreamSupport.stream(Arrays.asList(line.split(",")).spliterator(), false).map(Integer::parseInt).collect(Collectors.toList());
                        rows.add(row);
                        return true;
                    }

                    public List<List<Integer>> getResult() {
                        return rows;
                    }
                });
                List<Graph.Edge> edges = new ArrayList<>();
                for (int i = 0; i < matrix.size(); i++) {
                    List<Integer> row = matrix.get(i);
                    for (int j = 0; j < row.size(); j++) {
                        String r = String.valueOf(i);
                        String c = String.valueOf(j);
                        Integer cost = row.get(j);
                        if (i != 0) edges.add(new Graph.Edge(String.valueOf(i - 1) + "," + c, r + "," + c, cost));
                        if (j != 0) edges.add(new Graph.Edge(r + "," + String.valueOf(j - 1), r + "," + c, cost));
                    }
                }

                Graph graph = new Graph(edges.toArray(new Graph.Edge[edges.size()]));
                String start = "0,0";
                String end = (matrix.size() - 1) + "," + (matrix.get(0).size() - 1);
                graph.dijkstra(start);
                graph.printPath(end);
            }
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }
}
