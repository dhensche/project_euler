package org.dhensche;

import org.dhensche.problems.Problem81;

/**
 * @author dhensche
 *         Date: 8/10/15
 */
public class Solver {
    public static void main(String[] args) {
        new Problem81().solve();
    }
}
